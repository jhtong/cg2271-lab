CG2271 Lab 3 - Real Time Architectures II
=============================================



**Answer Book**

**Joel Haowen TONG** - *A0108165J*

**Wei Shen FONG** - *A0097979W*

## Question 1 (2 marks)

We see the numbers dequeued being printed to the Serial monitor, i.e.

    9
    8
    7
    6
    5
    4
    3
    2
    1
    0


## Question 2 (3 marks)

No, because the queue is a priority queue.

The queue has a function signature enq(queue, payload, priority), with the
highest priority being 0.

As the loop in `lab3part1a` iterates from 0 to 9, the corresponding priority in
each loop iteration goes from 9 to 0.  Hence, the first element added has the
lowest priority, and conversely.

Therefore, as the highest priority element is first dequeued, the values appear
to be reversed.

## Question 3 ( 4 marks)

The `(void *)` is a typecasting to a void pointer.  They are similar to templates
in C++, and is a form of polymorphism.  That is, the queue can be used for any
data type to be enqueued.  Separate queues do not have to be implemented for
each type - elements can differ in terms of type for 1 queue.  This allows the
queue to be reused for various functions, thus reducing the amount of code
compiled.


## Question 4 (4 marks)

The typecasting to `int` for the return value is intended for type safety.

The variable `val` is of type `int`.  Since the queue is not strictly type-casted
to a specific-type (shown in Question3), it can return a non-int value.  Thus,
   this should act as an added safety net against runtime type errors.


## Question 5 (3 marks)

Switch bouncing is the event whereby due to unclean contacts / physical
imperfections, a unclean button signal with a unclearly-defined edge is
produced.  It may appear as bounces.

In another case, as the human presses a switch too slowly, multiple interrupts
are triggered.  This may cause the switch to appear pressed multiple times,
    instead of once.

Suppose a switch toggle is given by:

    toggle = !toggle

A bounced switch may return 2 signals, leading to the toggle remaining in its
original state instead of being flipped.


## Question 6 (6 marks)

Debounce is first triggered by a user pressing a button.  It then calculates
the subsequent time interval that the button was pressed.  Should this duration
exceed an accepted number (\>500ms), then it is reasonable to accept that the
switch was pressed once.

To solve the switch bouncing problem, we debounce the switch by taking the
difference in times since it was pressed, and the time it was previously
pressed.  We make use of global variables `int0time` and `int1time` to store
the previously pressed time.

If the time is more than the threshold time (\>500ms), then the button is a
genuine press.  The respective global reference variables are then updated with
the *new* previously pressed time (now), and returns a 1.  Otherwise, the
detected button press might be due to bounce and hence erroneous; a 0 is hence
returned.

Thus, switch successfully returns a single press, instead of multiple presses -
it is debounced.


## Question 7 (5 marks)

### Enqueue

We would first have to typecast the function pointer to a definition that
matches the function signature `void(*funcptr)(void)`, before proceeding
to typecast the function pointer to `void*` to match the function signature of
the enqueue operation.  An example as follows:

    // useful typedef to avoid confusion
    typedef void (*funcptr)(void);

    // enqueue the function `int1task` with the priority 1.
    funcptr ptr = int1task;
    enq(queue, (void*)ptr, 1);


### Dequeue

To dequeue, the opposite is done.  Since deq returns a type `(void*)`, we
typecast it to the function signature `funcptr`, and append `()` to immediately
invoke the function:

    ( (funcptr)deq(queue) )();


## Question 8 (7 marks)

### Algorithm 

The generic algorithm is given as follow:

1. Attach ISR routine to program.
2. When triggered by button press, the respective ISR routine will be triggered.
3. The ISR routine will check for a genuine press via debouncing.  If so,
   enqueue the priority queue with the function pointer to the given action to
   be executed.
4. Upon availability (every cycle in the `loop` block), dequeue the
   highest-priority function pointer task in the queue, and invoke the function.
5. Rinse and repeat


### Implementation

#### Setup:

Attach hardware interrupt listeners to be triggered on `INT0` and `INT1`.
These hardware interrupts should call the ISR routine functions, when
triggered.

    void setup()
    {
        queue = makeQueue();
        attachInterrupt(0,int0ISR, RISING);
        attachInterrupt(1,int1ISR, RISING);
            
    }

#### ISR routines:

In the ISR routine, check if the switch is pressed by getting its value from
the invoked `debounce` function.  If it is pressed, enqueue the function
pointer to the *task to be executed*, with the given priority.  Like such, for
`INT1`:

    void int1ISR()
    {
        if (debounce(&int1time)) {
            funcptr ptr = int1task;
            //function signature enq(<queue>, <function_ptr>, <priority>);
            enq(queue, (void*)ptr, 1);
            
        }
    }

As `INT0` is encoded with a higher priority, this is realized by enqueing it
with a priority of 0, instead of 1.


#### Loop block:

Dequeue and execute when available.

    // Dequeues and calls functions if the queue is not empty
    void loop()
    {
        if (qlen(queue) > 0) {
            ((funcptr)deq(queue))();
            
        }
    }


### Solving of switch-bouncing problem:

To solve the switch bouncing problem, we debounce the switch by taking the
difference in times since it was pressed, and the time it was previously
pressed.  We make use of global variables `int0time` and `int1time` to store
the previously pressed time.

If the time is more than the threshold time (\>500ms), then the button is a
genuine press.  The respective global reference variables are then updated with
the *new* previously pressed time (now), and returns a 1.  Otherwise, the
detected button press might be due to bounce and hence erroneous; a 0 is hence
returned.


## Question 9 (3 marks)

`pin7` is triggered first, as the hardware interrupt `INT0` (controlling `pin7`)
    is first pressed and has a higher priority than `INT1`.  Hence in the
    queue, the function pointer task to flashing `pin7` is first dequeued.  
    
It is noted that the LED that flashes 5 times refers to `pin7` (given by
    the function `int0task`).




## Question 10 (3 marks)

The LED connected to `pin6` (triggered by hardware interrupt `INT1`) flashes
first, before the LED connected to `pin7`.  This is not intended as `INT0` has
a higher priority and hence should be triggered first, in the queue.  However,
  as `INT1` is always pressed first before `INT0`, and due to human reaction
  time, there is a lag time between the presses; a simultaneous press is
  *unfeasible*.  Hence in this case, `pin6` flashes first before `pin7`.


## Question 11 (5 marks)

No, it does not.  `pin7` almost always flashes first.  The only time the
reverse happens is in the first iteration of presses.  This anomaly is
explained in question 10.

In subsequent iterations however, we note that `pin7` flashes first.  This is
because the hardware interrupt `INT0` (controlling `pin7`) is given a higher
priority in the queue, than `INT1` (controlling `pin6`), and the function to
flash `pin7` is hence dequeued triggered first.  This is observed in the code:

    // for int0ISR:
    enq(queue, (void*)ptr, 0);

    //versus int1ISR:
    enq(queue, (void*)ptr, 1);

Where a lower number refers to a higher-priority task.


## Question 12 (5 marks)

### Algorithm:

1. The `pin6` should toggle 10 times per second, and `pin7` twice per second.  The
Lowest-Common Multiple (LCM) evaluates to 10.

2. Take the difference in time interval values.  If the time interval is more
   than 100ms (derived from 1000ms / 10 toggles per second), trigger the fast
   loop.  Keep track of the current loop iteration, modulo 10.

3. We note that slower loop is triggered twice per second.  Therefore, trigger
   the slower loop every 0th and 5th times.  (we note that iteration counter is
   periodic from `[0,9]` inclusive.)

4. On every 10th iteration, reset the counter back to 0.  We do not use a
   modulo operation, as it is supposedly slower than doing a reset at the end
   of the loop (necessary for time-critical operations).


### Implementation:

#### Main loop body:

    void loop()
    {
        unsigned long ref = 0;
        while(1) {
            if (millis() - ref >= 100) {
                fastLoop();
                ref = millis();
                loopCounter++;
            }
        }
    }


#### Fast loop:

    void fastLoop()
    {
        togglePin6();
        
        if (loopCounter == 10) {
            loopCounter = 0;
            
        }
        
        if (loopCounter == 0 || loopCounter == 5) {
            slowLoop();
        }
        
    }


#### Slower loop:

    void slowLoop()
    {
            togglePin7();
    }


#### The global loop counter used:

    unsigned int loopCounter = 0;
