#include <Arduino.h>

unsigned int loopCounter = 0;

void slowLoop();
void fastLoop();

void togglePin6()
{
	static char state=1;
	
	if(state)
	digitalWrite(6, HIGH);
	else
	digitalWrite(6, LOW);
	
	state=!state;
}


void togglePin7()
{
	static char state=1;
	
	if(state)
	digitalWrite(7, HIGH);
	else
	digitalWrite(7, LOW);
	
	state=!state;
}

void setup()
{
	pinMode(6, OUTPUT);
	pinMode(7, OUTPUT);
}

void fastLoop()
{
	togglePin6();
	
	if (loopCounter == 10) {
		loopCounter = 0;
		
	}
	
	if (loopCounter == 0 || loopCounter == 5) {
		slowLoop();
	}
	
}

void slowLoop()
{
	togglePin7();
}

void loop()
{
	unsigned long ref = 0;
	while(1) {
		if (millis() - ref >= 100) {
			fastLoop();
			ref = millis();
			loopCounter++;
		}
	}

}

int main()
{
	init();
	setup();
	
	while(1)
	{
		loop();
		if(serialEventRun)
		serialEventRun();
	}
}