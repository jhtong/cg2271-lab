CG2271 Laboratory Report
=======================================

| Name | Matric No. |
|--------|--------|
| Fong Wei Shen	| A0097979W |
| Joel Tong	| A0108165 |

## \# 1 

Functions are segments of code that are executed on the stack, are then popped, and their return values inserted into the relevant memory locations in the stack or heap.

In a normal function, a function executes in a sequence similat to the below:

1. The calling code on the stack first pushes memory space on the stack for return values.
2. The memory address of the calling code is stored.  This is known as the **frame pointer** and remains constant during the period of execution.
3. The function code is loaded onto the stack in memory.  The memory address of the current executing code is stored by the **stack pointer**, and is free to change during execution.  Arguments are loaded by **registers**.
4. When done, the result is stored in the return result memory space, pointed by the **frame pointer**.
5. The function code is popped from the stack.  Execution continues.

A function pointer operates in a similar fashion.  However, it is loaded onto the heap instead, like a normal dynamically allocated data type.  When dereferenced, execution continues in a similar fashion seen from the above.

At every instance when the function pointer is called, however, a new context saving space is created on the stack for the function to execute.  Thus should the function be pre-empted, the context is preserved and the stack pointer shifted to the next executing task.  Once this task is resumed (by shifting the stack pointer back), the task using the same function pointer continues, using the data stored in the personal context space.

In addition, tasks 1 and 2 operate well despite using the same function pointer, as they are *re-entrant* (and hence thread-safe).  This is because the function pointer does not manipulate any global variables, and hence will not interfere with any data outside its context space.


## \# 2

We would *expect* the Uno to execute task0 and task1 in sequence, and therefore print the following messages (as Task1 has a higher priority):

	Task 1
    Task 2
    Task 1
    Task 2

However, because the Arduino `Serial` library is not thread-safe, and because `Serial` is slow and can hence be pre-empted before it finishes, we would expect output to become garbled along the way.

## \# 3

The Arduino `Serial` library is not thread-safe.  Therefore, writing from another thread can garble the input.  In addition, `Serial` is slow.  There is an average waiting time of 2.5ms before data from the next process is sent to the Serial class, which is far faster than what Serial can print out at 9600 baud from a single task, in its entirety.  Therefore, the message appears garbled.

## \# 4

	void OSCreateQueue(	int *buffer,
    					unsigned char length,
                        OSQueue *queue)

#### Purpose 

This initializes the queue in the structure pointed by `*queue`.

#### Arguments 

- `int *buffer` - A reference to the buffer used to store queue messages.
- `unsigned char length` - The length of the buffer.
- `OSQueue *queue` - A reference to the queue structure.

#### Returns 

Nothing.


	void OSEnqueue(	int *data,
                    OSQueue *queue)


#### Purpose 

Inserts data into the queue.

#### Arguments 

- `int *data` - A reference to the data instance to store
- `OSQueue *queue` - A reference to the queue structure.

#### Returns 

Nothing.


	int OSDequeue(OSQueue *queue)

#### Purpose 

Removes and returns the oldest element in the FIFO queue.  If the queue is empty, calls to `OSDequeue` block until data is available.

#### Arguments 

- `OSQueue *queue` - A reference to the queue structure.

#### Returns 

The removed entry that was formerly the oldest inserted element.

| Queues in Data Structures | Queues in ArdOS |
|--------|--------|
| They do not block when empty. | Queues block tasks when empty, until data is available. |
| Queues are typically priority-agnostic and are FIFO. | Priority queues are supported on ArdOS, and less crucially in FIFO order |
| If the queue is implmented using pointer nodes, the queue might be of variable size. | The queue has a fixed length defined by the size of the buffer |
| Queues are typically thread-safe and work across tasks | Queues are typically used by single-process systems, in a given scope.


## \# 5

	Task 1
    Task 2
    Task 1
    Task 2
    ... ...
    Task 1
    Task 1
    Task 1
    ... ...


## \# 6

The Arduino `Serial` class has an internal buffer to store messages, before it is printed to the Serial port, as I/O operations tend to be very slow (especially at 9600 baud).

If there is too much info pushed to the Serial instance, its internal buffer may overflow and partial bits may be written as a result.  This leads to garbled input.

We observe that `OSSleep` allows the task to "rest" for 50ms.  This is sufficient time to allow data from the internal Serial buffer to be written to the serial port.  If this was removed, too much information might be pushed, resulting in buffer overflow.

## \# 7
Task 1 is given a higher priority over task 2.  Hence, when enqueued, task 1 preceedes task 2 (when available).

Tasks are printed out as expected, when there is space in the queue for enqueuing task 1 and task 2.

However, as time progresses, the memory buffer becomes full with task 1 functions (as task 1 has a higher priority over task 2) and hence only task 1 is allowed to run instead of alternating between task 1 and 2.

## \# 8
	void OSCreateBarrier(unsigned int count, struct OSBarrier *barrier)
	{
  		barrier->count = count;
  		OSCreateSema(&(barrier->sema), 0, 0);
	}

#### Overview

Our implementation involved using a countdown counter to keep track of the number of tasks that have reached the barrier.  Upon reaching the barrier, the counter is decremented.  All tasks are only allowed to proceed upon reaching zero.

Semaphores were used to facilitate in co-ordinating the tasks.  An initial binary semaphore (initialized to unavailable) was created.  When tasks reached the barrier, count was decremented, and subsequntly blocked by `OSTakeSema()`.  Tasks were only allowed to proceed via `OSGiveSema`, when all tasks reached the barrier (`count == 0`).

#### OSCreateBarrier

OSCreateBarrier takes in 2 parameters. This 1st parameter is the number of tasks and the 2nd barrier is the object pointer to the object itself.
The barrier->count will be initialised to the number of tasks that is running. 

OSCreateSema will create a binary semaphore in the OSBarrier structure.

## \# 9

	void OSReachBarrier(struct OSBarrier *barrier) 
	{
  		barrier->count--;
  		while ((barrier->count) == 0)
    		OSGiveSema(&(barrier->sema));
  		if (barrier->count > 0)
    		OSTakeSema(&(barrier->sema));

  		OSSleep(100); 
	}

When called, OSReachBarrier decrements the internal count in `barrier` by 1, signifying that the calling task has reached the barrier.  This barrier is a common task shared between all tasks.  Should `count` be more than zero (there remains tasks that have not reached the barrier), `OSTakeSema()` is called.  As the binary semaphore is in an unavailable state, this call blocks the calling task, and forces the task to pend on the semaphore according to the task's priority.  To prevent tasks from blocking the entire execution process and hence all other tasks from running, the task yields by making a call to `OSSleep()`.

However, if all tasks have reached the barrier (`count == 0`), The semaphore is forced to be made available by calls to `OSGiveSema()`  in an indefinite loop. All blocked tasks (Tasks that have reached the barrier) are now allowed to continue.


## \# 10
It is because tasks, when blocked, are enqueued in priority.  The task was executed in the order 2,3,1 but however, task 3 has a OSSleep of 850 as compared to task 1 of 250. Hence task 1 reach the barrier before task 3. But when resuming the tasks to cross the barrier, it will resume back to the priority of the tasks and cross the barrier in this sequence, 2,3,1. 
