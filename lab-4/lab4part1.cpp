// #include <avr/io.h>
// #include <Arduino.h>
#include <kernel.h>

#define NUMTASKS 2

void task1(void* arg) {
    while(1) {
        digitalWrite(6, HIGH);
        OSSleep(100);
        digitalWrite(6, LOW);
        OSSleep(100);
    }

}

void task2(void* arg) {
    while(1) {
        digitalWrite(6, HIGH);
        OSSleep(500);
        digitalWrite(6, LOW);
        OSSleep(500);
    }

}

void setup() {
    pinMode(6, OUTPUT);
    pinMode(7, OUTPUT);

    OSInit(NUMTASKS);

    OSCreateTask(0, task1, NULL); 
    OSCreateTask(1, task2, NULL);

    OSRun();
}


void loop() {}
