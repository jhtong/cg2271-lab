/*
 * lab4part3.cpp
 *
 * Created: 4/10/2013 6:19:58 PM
 *  Author: dcstanc
 */ 


/**
 *
 * @author jhtong, Wei Shen
 *
 * Manage resources using semaphores.
 *
 *
 * == General flow: Uses binary semaphores ==
 *
 * 1. Pin receives interrupt.  Yay!  Raises volatile flag via ISR 
 *
 * 2. For each task, on loop entry, check if volataile flag raised.  If yes,
 * unset flag and take resource (down counter).  If counter is 0 (NOT AVAILABLE
 * BOO), block and enqueue current task until resource is ready (counter > 0). 
 *
 * 3. When done executing, release resource (up counter).
 *
 * 4. Each task must obey its own (fixed?) priority in the queue.
 *
 * 5. Rinse and repeat.
 *
 */


#include <avr/io.h>
#include <Arduino.h>
#include <kernel.h>
#include <sema.h>
#include <mutex.h>

unsigned long int0time=0, int1time=0;
OSSema task1Go, task2Go;

OSMutex mutex;


// Debouncing function. Returns TRUE if this interrupt was not caused by a bouncing switch
int debounce(unsigned long *debTimer)
{
	unsigned long tmp=*debTimer;
	unsigned long currTime=OSticks();
	
	if((currTime-tmp) > 500)
	{
		*debTimer=currTime;
		return 1;
	}
	else
	return 0;
	
}


void task1(void *param) 
{
	while(1) {
        // set flag implies button pressed.  enqueue operation via semaphore
        // (if need be) and reset the flag to false.

            // blocks and enqueues if unavailable
            OSTakeSema(&task1Go);
            OSTakeMutex(&mutex);

            for(int i=0; i<2; i++) {
                digitalWrite(6, HIGH);
                OSSleep(1000);
                digitalWrite(6, LOW);
                OSSleep(1000);
            }

            // release resource 
            OSGiveMutex(&mutex);

        }

}


void task2(void *param) 
{
	while(1) {
        //OSPrioSwap();
        //OSSleep(1);
        // set flag implies button pressed.  enqueue operation via semaphore

            // blocks and enqueues if unavailable
            
            OSTakeSema(&task2Go);
            OSTakeMutex(&mutex);

            for(int i=0; i<5; i++) {
                digitalWrite(7, HIGH);
                OSSleep(250);
                digitalWrite(7, LOW);
                OSSleep(250);
            }

            // release resource 
            OSGiveMutex(&mutex);

    }
}


void int0ISR()
{
	if (debounce(&int0time)) {
        // set flag HEY! I AM PRESSED!!
        
          OSGiveSema(&task1Go);
	}
	
}


void int1ISR()
{
	if (debounce(&int1time)) {
        // set flag HEY! I AM PRESSED!!
          OSGiveSema(&task2Go);
	}
	
}


void setup()
{
	// Set pins 6 and 7 as output

	// Initialize the OS
	OSInit(2);
Serial.begin(9600);
	pinMode(6, OUTPUT);
	pinMode(7, OUTPUT);
	// Create the binary semaphores
    // initially: ALL RESOURCES AVAILABLE (1)
	OSCreateSema(&task1Go, 0, false);
	OSCreateSema(&task2Go, 0, false);

        OSCreateMutex(&mutex);
	// Add in the tasks
	OSCreateTask(0, task1, NULL);
	OSCreateTask(1, task2, NULL);
	
	attachInterrupt(0,int0ISR,RISING);
	attachInterrupt(1,int1ISR,RISING);
	// Launch the OS
	OSRun();

}


void loop()
{
	// Empty
}

// Do not modify
int main()
{
	init();
	setup();

	while(1)
	{
		loop();
		if(serialEventRun) {
            serialEventRun();

        }
	}
}
