/*
 * lab4part3.cpp
 *
 * Created: 4/10/2013 6:19:58 PM
 *  Author: dcstanc
 */


/**
 *
 * @author jhtong, Wei Shen
 *
 * Manage resources using semaphores.
 *
 * @brief Multitasker that blinks 2 different LEDs when buttons connected to
 * INT0 and INT1 on the Arduino are triggered, via interrupts.  Implementation
 * on ArdOS.
 *
 * This implementation uses semaphores for flow control.  A semaphore is a flag
 * that details the number of resources left, for a particular task(s).  A
 * positive number denotes that resources are available.
 *
 * A resource number of 0 (or negative number) for a semaphore implies that the
 * resource is unavailable.  In this case, the calling task for the resource is
 * blocked, until resources are available to resume the task.  
 *
 * A semaphore that allows only 0 or 1 is a binary semaphore.  A semaphore that
 * allows values more than 0 or 1 is a counting semaphore.
 *
 * In our implementation, each task is controlled by a semaphore.  These tasks
 * are greedy, that is, they will consume a resource as soon as it is
 * available, until no resource is left.  These tasks blink the LEDs.  
 *
 * Each semaphore is set to a value of 0, making them unavailable.  Upon a
 * button press interrupt event, an interrupt is triggered and an ISR is
 * called.  This ISR increments the semaphore counter, corresponding to the
 * number of times the button is pressed.  Thus, each resource count signifies
 * the number of times the LED should blink with that particular pattern (5
 * times of 250 ms).  The semaphore used is a counting semaphore, and resource
 * count is decremented in the task block.
 *
 * Semaphores have to be used, rather than checking for volatile flags, as
 * these semaphore operations are designed to be atomic, i.e. they are
 * thread-safe in ArdOS.  Should volatile boolean flags be used instead, the
 * program flow will not work as desired due to multiple pre-emptions as the OS
 * jumps between tasks.
 *
 * As observed, the flow of LEDs appears to happen in parallel due to the
 * Operating system, rather than in sequence.  Should a sequential operation be
 * desired instead, mutexes with conditionals, or some flow of sequenced
 * operations (e.g. promises and chains) should be more appropriate instead.
 *
 *
 */


#include <avr/io.h>
#include <Arduino.h>
#include <kernel.h>
#include <sema.h>

// for debouncing 
unsigned long int0time=0, int1time=0;
// semaphores used
OSSema task1Go, task2Go;


/* ----------------------------------------------------------------*/
/**
 * @brief Debouncing function. Returns TRUE if this interrupt was not caused by
 * a bouncing switch
 *
 * @param debTimer
 *
 * @return 
 */
/* ------------------------------------------------------------------*/
int debounce(unsigned long *debTimer)
{
    unsigned long tmp=*debTimer;
    unsigned long currTime=OSticks();

    if((currTime-tmp) > 500)
    {
        *debTimer=currTime;
        return 1;
    }
    else
        return 0;

}


/* ----------------------------------------------------------------*/
/**
 * @brief block for task1 
 *
 * @param param
 */
/* ------------------------------------------------------------------*/
void task1(void *param)
{
    while(1)
    {
        // decrement semaphore resource counter
        OSTakeSema(&task1Go);

        for(int i=0; i<5; i++)
        {
            digitalWrite(6, HIGH);
            OSSleep(250);
            digitalWrite(6, LOW);
            OSSleep(250);
        }
    }

}


/* ----------------------------------------------------------------*/
/**
 * @brief block for task2 
 *
 * @param param
 */
/* ------------------------------------------------------------------*/
void task2(void *param)
{
    while(1)
    {
        // decrement semaphore resource counter
        OSTakeSema(&task2Go);

        for(int i=0; i<5; i++)
        {
            digitalWrite(7, HIGH);
            OSSleep(250);
            digitalWrite(7, LOW);
            OSSleep(250);
        }
    }
}


/* ----------------------------------------------------------------*/
/**
 * @brief ISR block for INT0.  Increments respective semaphore counter 
 */
/* ------------------------------------------------------------------*/
void int0ISR()
{
    if (debounce(&int0time))
    {
        // Give semaphore resource - do "up" operation
        OSGiveSema(&task1Go);
    }

}


/* ----------------------------------------------------------------*/
/**
 * @brief ISR block for INT1.  Increments respective semaphore counter 
 */
/* ------------------------------------------------------------------*/
void int1ISR()
{
    if (debounce(&int1time))
    {
        // Give semaphore resource - do "up" operation
        OSGiveSema(&task2Go);
    }

}


/* ----------------------------------------------------------------*/
/**
 * @brief Block for setup, initializes OS
 */
/* ------------------------------------------------------------------*/
void setup()
{
    // Set pins 6 and 7 as output

    // Initialize the OS
    OSInit(2);

    // set pinMode here.  Note that setting it before OSInit() will not work.
    pinMode(6, OUTPUT);
    pinMode(7, OUTPUT);

    // Create the binary semaphores
    // set resources to be initially unavailable
    OSCreateSema(&task1Go, 0, false);
    OSCreateSema(&task2Go, 0, false);

    // Add in the tasks.  task1 has higher priority than task 2.
    OSCreateTask(0, task1, NULL);
    OSCreateTask(1, task2, NULL);

    // attach interrupts to trigger blinks on press
    attachInterrupt(0,int0ISR,RISING);
    attachInterrupt(1,int1ISR,RISING);

    // Launch the OS
    OSRun();

}


void loop()
{
    // Empty
}


// Do not modify
int main()
{
    init();
    setup();

    while(1)
    {
        loop();
        if(serialEventRun)
        {
            serialEventRun();

        }
    }
}
