#include <stdio.h> 
#include <math.h> 
#include <time.h> 
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define NUMELTS 16384 
// IMPORTANT: Compile using "gcc lab6c.c .lm -o lab6c". 
// The "-lm" is important as it brings in the Math library. 
// Implements the naive primality test. 
// Returns TRUE if n is a prime number 
int prime(int n) 
{ 
	int ret=1, i; 
	for(i=2; i<=(int) sqrt(n) && ret; i++) 
		ret=n % i; 
	return ret; 
} 
int main() 
{ 
	int data[NUMELTS];
	int fd[2];
	int count = 0, buffer, i; 
	pid_t pid;
	pipe(fd)	;

	// Create the random number list. 
	srand(time(NULL)); 
	for(i=0; i<NUMELTS; i++) 
		data[i]=(int) (((double) rand() / (double) RAND_MAX) * 10000); 
	// Now create a parent and child process. 
	//PARENT: 
	if(pid=fork())
	{
		for(i = 0; i < 8192; i++)
		{
			if(prime(data[i]))
				count++;
		}
		wait();
		printf("Number of prime found by parent is %d\n", count);
		close(fd[1]);
		read(fd[0], &buffer, sizeof(buffer));
		printf("Number of prime found by child is %d\n", buffer);
		printf("Total number of prime found is %d\n", count + buffer);
	}
	else
	{
		for(i = 8192; i < NUMELTS; i++)
		{
			if(prime(data[i]))
				count++;
		}
		close(fd[0]);
		write(fd[1], &count, sizeof(count)); 
	}

} 
