# CG2271 Lab 6 Homework

| Name | Matric No. |
|--------|--------|
| Fong Wei Shen	| A0097979W |
| Joel Tong	| A0108165 |

## Question 1

A PID (process identifier) is a unique number used by the operating system kernel to identify a process.  A unique PID is assigned to each new task.  PIDs are given based on a counter value.  The counter value is cycled upon reaching a maximum value.

PID 0 is reserved for paging (a kernel operation), while PID 1 is reserved for init (the process starting and shutting down the system).

PIDs are used for terminating processes, and changing the priority of processes.

Parent of *Lab6a's parent process*: 1

## Question 2

Yes, the values of `k` between the child and parent are independent.

The calling of `fork()` creates an exact copy of the parents' address space data and runs it.  Since k was initialized with a value of 15 before `fork()`, the value of `k` is copied and run independently in separate processes.


## Question 3

`PARENT_DELAY` and `CHILD_DELAY` are arguments to `sleep()`.  `sleep` controls the duration of time a process is paused.

Since both tasks are running simultaneously, both parent and child outputs become mixed on the screen, with the delay determined by the value of `sleep` passed.

Yes, the child process continues to execute.  That is because the parent does not pend on the child to complete its process, before terminating - it is independent..

## Question 4

    if(pid=fork())
	{
		printf("Process  ID of child: %d\n", pid);

		for(i=0; i<10; i++)
		{
			printf("Parent: i=%d k=%d\n", i, k);
			k++;
			//Delay
			sleep(PARENT_DELAY);
		}
		wait();
	}

`wait()` was added at the end of the for loop for the parent process in order to instruct the program to wait for the child process to finish its task before exiting to the shell.

## Question 5

	Parent send message: Hello child! and 128
    
Process is forked and 2 messages are sent to the child and displayed on the screen via a pipe.

## Question 6

The `sizeof()` function returns the size of the variable, allocated in memory (i.e. byte size).

## Question 7

	#include <stdio.h> 
	#include <math.h> 
	#include <time.h> 
	#include <stdlib.h>
	#include <unistd.h>
	#include <string.h>
	
	#define NUMELTS 16384 
	// IMPORTANT: Compile using "gcc lab6c.c .lm -o lab6c". 
	// The "-lm" is important as it brings in the Math library. 
	// Implements the naive primality test. 
	// Returns TRUE if n is a prime number 
    
    
	int prime(int n) 
	{ 
		int ret=1, i; 
		for(i=2; i<=(int) sqrt(n) && ret; i++) 
			ret=n % i; 
		return ret; 
	} 
    
    
	int main() 
	{ 
		int data[NUMELTS];
		int fd[2];
		int count = 0, buffer, i; 
		pid_t pid;
		pipe(fd);
	
		// Create the random number list. 
		srand(time(NULL)); 
		for(i=0; i<NUMELTS; i++) 
			data[i]=(int) (((double) rand() / (double) RAND_MAX) * 10000); 
		// Now create a parent and child process. 
        
		// PARENT: 
		if(pid=fork())
		{
			for(i = 0; i < 8192; i++)
			{
				if(prime(data[i]))
					count++;
			}
			wait();
			printf("Number of prime found by parent is %d\n", count);
			close(fd[1]);
			read(fd[0], &buffer, sizeof(buffer));
			printf("Number of prime found by child is %d\n", buffer);
			printf("Total number of prime found is %d\n", count + buffer);
		}
        
        // CHILD:
		else
		{
			for(i = 8192; i < NUMELTS; i++)
			{
				if(prime(data[i]))
					count++;
			}
			close(fd[0]);
			write(fd[1], &count, sizeof(count)); 
		}
	} 
