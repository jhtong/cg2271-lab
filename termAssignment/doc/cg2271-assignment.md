# CG2271 Real Time Operating Systems<br>2013/14 Semester I<br>Term Assignment Report

| Name | Matric No. |
|--------|--------|
| Joel Tong	| A0108165J |
| Fong Wei Shen	| A0097979W |

---------------------------



## Section 1 – Implementation

### Cut and paste your implementation for first fit here:

    // Search for free memory using first fit policy
    TMemoryNode *curr = _head;

    // return the first fit found.  Worst case is end of list  
    // (not found, full).
    while(curr) {
        if (curr->len >= requestedLen && curr->allocated == 0) {
            return curr;
        }
        curr = curr->next;
    }
    return NULL;

#### Briefly explain your implementation:

The first fit implementation is similar to a greedy algorithm.  For each node, check that the size of the current node is greater than or equal to the size requested.  If it is, exit the loop and return the node.  If no such node is found, then return a NULL.


### Cut and paste your implementation for best fit here:

    // Search for free memory using best fit policy

    TMemoryNode *curr = _head, *best = NULL;


    /** first find the first fit of best. **/
    while(curr) {
        if (curr->len >= requestedLen && curr->allocated == 0) {
            best = curr;
            break;
        }
        curr = curr->next;
    }

    /** If best is non-existent, it is not in list. **/
    if (best == NULL) return NULL; 
    
    /**
     * Since program execution proceeds here, the program definitely has a free
     * memory space.  Try to optimize.
     * Continue iteration if at least 1 suitable node is found.
     * Note that this node must be at least allocated length long.
    **/
    while(curr) {
        int isBetter = (curr->len < best->len) && 
            (curr->len >= requestedLen);

        if (isBetter && curr->allocated == 0) {
            best = curr;
        }

        curr = curr->next;
    }

    return best;

#### Briefly explain your implementation:

There are 4 parts to the implementation:

1. First, traverse and find one node that matches the specification (similar to first-fit implementation).  If such a node is found, break and store that as the `best`.

2. No node found implies that the memory map is full.  If it is full, return `NULL`.  If it is not, traverse the remaining `N - i` nodes to find a better fit.

3. If a new node is found such that it meets the specifications, but utilizing a lesser memory block, replace the value of `best` with the node.

4. At the end of traversal, return the `best` node.


### Cut and paste your implementation for worst fit here:

    // Search for free memory worst fit policy
    TMemoryNode *curr = _head, *worst = NULL;


    /** first find the first fit of worst. **/
    while(curr) {
        if (curr->len >= requestedLen && curr->allocated == 0) {
            worst = curr;
            break;          // optimize, do not waste time!!
        }
        curr = curr->next;
    }

    /** If worst is non-existent, it is not in list. **/
    if (worst == NULL) return NULL; 
    
    /** 
     * Continue iteration if at least 1 suitable node is found, since it must
     * definitely be in the list 
    **/
    while(curr) {
        int isWorse = curr->len > worst->len;

        if (isWorse && curr->allocated == 0) {
            worst = curr;
        }

        curr = curr->next;
    }

    return worst;

#### Briefly explain your implementation:

It is the opposite of first fit.  The problem is reduced to finding the largest available memory block that meets the specification.  There are 4 parts to the implementation:

1. First, traverse and find one node that matches the specification (similar to first-fit implementation).  If such a node is found, break and store that as the `worst`.

2. No node found implies that the memory map is full.  If it is full, return `NULL`.  If it is not, traverse the remaining `N - i` nodes to find a worse fit.

3. If a new node is found such that it meets the specifications, but utilizing a lesser memory block, replace the value of `worst` with the node.

4. At the end of traversal, return the `worst` node.

-------------------------

## SECTION 2 – EXPERIMENT 1 : RUNNING TIMES

### 1. Record your readings below:

Running times:

|  Algo   | Run1 Time | Run2 Time | Run3 Time | Run4 Time | Run5 Time | Average |
|--|--|--|--|--|--|--|
|First Fit|   5.03    |   5.42    |	  8.57    |	  6.76    |   6.46    |  6.448  |
|Best Fit |   8.50    |	  5.81    |	  5.73    |   8.28    |	  8.55    | 7.374   |
|Worst Fit|   8.81    |	  8.99    |	  8.76    |   8.57    |	  8.82    | 8.79    |

**Running time measured in seconds**


Variance:

|  Policy | Run1 Var | Run2 Var   | Run3 Var | Run4 Var | Run5 Var | Average |
|--|--|--|--|--|--|--|
|First Fit|   19.08  |    22.38   |	  51.18  |	29.11   |  33.36   | 31.022  |
|Best Fit |   50.65  |	  26.67   |	  18.36  |  47.06   |  51.09   | 38.766  |
|Worst Fit|   54.30  |	  56.80   |	  53.35  |  50.45   |  54.10   | 53.80   |

**A smaller variance indicates more constant readings**


### 2. Is there an algorithm that is faster than the others? If there is an algorithm that is faster explain why. If not, explain why as well.

Among the three algorithms, First Fit is the fastest, followed by Best Fit, and then lastly Worst Fit. 

First fit is faster because it does not take into consideration memory wastage. First Fit will	scan through the bit map and find the first block of free units which can fit the requested size. First Fit will stop searching after the first occurence of locating a block of memory which can fit the requested size.  Hence, it does not iterate through all `N` nodes in the best / average case.

In the case of Best Fit however, it involves searching through the whole bit map. Once it locates a block of memory which can fit the requested size, it will store it for future comparision with the next block of memory located. It will then take the smallest block which can fit the requested size.  Hence, running time is fairly constant as it traverses all `N` nodes.

As for Worst Fit, it is the vice versa to Best Fit. Instead of locating the smallest suitable block, it will located the largest block of free memory.  First Fit will stop searching after it locates the first free block of memory.  Hence the running time is constant, traversing all `N` nodes.

Hence, First Fit is the fastest.

#### Further discussion

Variance gives the extent of variation of timings of all results used to calculate the respective means.  The larger the number, the more varied timings are.  It is observed that worst fit has a significantly higher variance.  However, while both Best Fit and Worst Fit have similar running times, differing only in timing, this disparancy in variance stenghtens the possibility that there were minor differences between both Best Firt and Worst Fit mean timings due to short-circuit logic favoring biases in data, as well as varied performances at different memory locations and run times in the Operating System.

There were slight discrepancies of 2 seconds, in certain points, used for the calculation of timings of the average for both First Fit and Best Fit implementations.  This disparancy in individual points could be attributed to the randomness in traversing discontiguous locations in memory in linked lists.

### 3. External fragmentation is given by:

<img src="http://upload.wikimedia.org/math/9/5/2/952055f7a03a5570cbde5e73193e1800.png" style="width: 600px;"/>

*If this formula returns a value of 0.3, what is the significance of this number with regards to memory allocation?*

Using sets and percentages, the universal set refers to the total free memory.  Let this percentage be *1.00*.  Then let the largest block of free memory be *A*, with the percentage of free space taken be *x* such that *1 >= x*

*A* can be found by diving the largest block of free memory, over the total free memory available.

Then let *B* refer to the percentage of memory taken up by all other free blocks.  By law of excluded middle, *B = 1 - x*.

Since *B* refers to all other free blocks scattered elsewhere in memory, a number of *0.3* implies that *30%* of all available memory is in smaller, discontiguous blocks (i.e. fragmented).  The smaller the percentage of external fragmentation, the better.

### 4. Record the readings from Experiment 2 below:

|  Policy  | Total Free Memory  | Largest Free Memory | External Fragmentation |
|--|--|--|--|
|First Fit |         20         |         8           |          0.6           |
|Best Fit  |         20         |         8           |          0.6           |
|Worst Fit |         20         |         8           |          0.6           |

### 5. Based on your readings above comment on the effectiveness of each policy at minimizing external fragmentation. Explain your answer based on what you know of each policy.

The effectiveness at minimizing external fragmentation is the same for all 3 policies - not much. Having an external fragmentation of 0.6 implies that 60% of all available memory is in smaller, discontiguous blocks (i.e. fragmented). This is not effective and degrades the performance of the system.

All three policies cause remaining free memory blocks to be discontiguous, making it impossible to coalesce memory blocks, therefore causing them to be unusable. This encourages external fragmentation and should be minimized by further tweaking the algorithms (e.g. using Buddy allocation instead, which is a much more efficient way to handle memory with lower external fragmentation).

It is noted that First Fit should have the worst external fragmentation of the three, however this does not correlate with out findings.  In addition, Worst Fit should have lower external fragementation as allocating the largest block of available memory should reduce the number of *holes* in memory.  However, both these behaviors were not observed.  A future study involving larger memory space with more varied numbers should be conducted to observe this behavior.

To reduce the level of external fragmentation, it may be wise to use the Worst Fit policy, in addition to running a program that ensures all memory is constantly contiguous, by copying fragmented segments into contiguous segments in memory.  An example of such a program is a *Copying Collector*.
