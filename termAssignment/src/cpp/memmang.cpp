#include <stdlib.h>
#include <limits.h>
#include "memmang.h"
#include "linkedList.h"
#include "nodes.h"

// set to 1 if in Debug mode
#define DEBUG_MODE 1

/* -----------------------------------------------------------------
 
 Memory Management Utilities
 ---------------------------
 
 These are provided to help you build your memory manager. You do not
 need to modify the codes given here.
 
 ----------------------------------------------------------------- */

extern TMemoryNode *_head;

/* -----------------------------------------------------------------
 
 Memory Allocation Routines
 --------------------------
 
 These are the actual routines you will implement. You should modify only
 findFreeMemory, NSAlloc and NSFree.
 
 ----------------------------------------------------------------- */

// The actual memory to allocate
char _memoryBlock[MEMMANG_MAXSIZE];
unsigned long _allocated_length;

void initializeMemory()
{

    // DO NOT MODIFY THIS CODE
    
    // Initialize the list of nodes
    createList(256);
    
    // Initializes the memory manager with a single node consisting of free memory
    // the size of memmang_maxsize, starting at location zero.
    TMemoryNode *myNewNode=newNode(0, MEMMANG_MAXSIZE, 0);
    myNewNode->startAddress=MEMMANG_BASEADDR;
    insertNode(myNewNode);
}

long double NSGetLastAllocLen()
{
    return _allocated_length;
}


/* -------------------------------------------
 
    Modify only code below this line 
 
    ------------------------------------------- */


/* ----------------------------------------------------------------*/
/**
 * @brief This function finds a suitable memory space, based on either of three
 * algorithms: best, worst, and first fit.
 *
 * Under the best fit algorithm, model returns the largest available memory
 * space to suit the task available.
 *
 * Under the worst fit algorithm, model returns the smallest available memory
 * space, that is greater than or equal to the requirement, to suit the task available.
 *
 * Under first fit algorithm, the first space that is found to support the
 * requirement is returned.
 *
 * CONTRACT: This algorithm assumes that the memory map has been coalesced as
 * much as possible (i.e. there are no adjacent free spaces).
 *
 * Pre: requestedLen = # of bytes of memory requested
 * Post: Returns pointer to memory management node (TMemoryNode) describing the available memory
 *
 * @param requestedLen The requested length of free memory space, in bytes, to
 * search for.
 *
 * @return A TMemoryNode pointer to the memory space found.
 */
/* ------------------------------------------------------------------*/
TMemoryNode *findFreeMemory(unsigned long requestedLen)
{

    /* TODO: Implement algorithm to locate next free memory block
             using the allocation policy indicated */
    
    // ENSURE THAT YOUR CODE IS PROPERLY DOCUMENTED!!
    
#if MEMMANG_TYPE==MEMMANG_BESTFIT
    // Search for free memory using best fit policy

    TMemoryNode *curr = _head, *best = NULL;


    /** first find the first fit of best. **/
    while(curr) {
        if (curr->len >= requestedLen && curr->allocated == 0) {
            best = curr;
            break;
        }
        curr = curr->next;
    }

    /** If best is non-existent, it is not in list. **/
    if (best == NULL) return NULL; 
    
    /**
     * Since program execution proceeds here, the program definitely has a free
     * memory space.  Try to optimize.
     * Continue iteration if at least 1 suitable node is found.
     * Note that this node must be at least allocated length long.
    **/
    while(curr) {
        int isBetter = (curr->len < best->len) && 
            (curr->len >= requestedLen);

        if (isBetter && curr->allocated == 0) {
            best = curr;
        }

        curr = curr->next;
    }

    return best;

#elif MEMMANG_TYPE==MEMMANG_WORSTFIT
    // Search for free memory worst fit policy
    TMemoryNode *curr = _head, *worst = NULL;


    /** first find the first fit of worst. **/
    while(curr) {
        if (curr->len >= requestedLen && curr->allocated == 0) {
            worst = curr;
            break;          // optimize, do not waste time!!
        }
        curr = curr->next;
    }

    /** If worst is non-existent, it is not in list. **/
    if (worst == NULL) return NULL; 
    
    /** 
     * Continue iteration if at least 1 suitable node is found, since it must
     * definitely be in the list 
    **/
    while(curr) {
        int isWorse = curr->len > worst->len;

        if (isWorse && curr->allocated == 0) {
            worst = curr;
        }

        curr = curr->next;
    }

    return worst;
    
#elif MEMMANG_TYPE==MEMMANG_FIRSTFIT
    // Search for free memory using first fit policy
    TMemoryNode *curr = _head;

    // return the first fit found.  Worst case is end of list  
    // (not found, full).
    while(curr) {
        if (curr->len >= requestedLen && curr->allocated == 0) {
            return curr;
        }
        curr = curr->next;
    }
    return NULL;
    
#endif

    // dummy return to suppress compiler errors. Modify to return a pointer
    // to the TMemoryNode memory management node describing the free memory found.
    return NULL;
    
}

/* ----------------------------------------------------------------*/
/**
 * @brief Auxiliary function to print memory map, for debugging.
 */
/* ------------------------------------------------------------------*/
void printMemoryMap() {
    TMemoryNode *curr = _head;
    while(curr != NULL) {
        printf("Address: %x | Prev: %x | Curr: %x | Next: %x | length: %lu | allocated: %i\n",
            curr->startAddress, curr->prev, curr, curr->next, curr->len, curr->allocated);
        curr = curr->next;
    }
}


/* ----------------------------------------------------------------*/
/**
 * @brief Allocates a block of memory to the function.
 *
 * Internally, this is done by first finding for a suitable memory slot (based
 * on either of the 3 methods), before the memory slot is split (remaining
 * memory is returned, used memory is allocated).  Remaining available memory
 * in that memory slot, if available, always occurs towards the tail of the
 * memory map.
 *
 * Pre: requestedLen = Amount of memory requested in bytes
 * Post: Memory is allocated and address of the start of the memory segment allocated is
 *      returned. Return a NULL if there is no more memory to allocate.
 *
 * @param requestedLen The requested length of memory, in bytes, to allocate
 *
 * @return the address of the start of the memory segment allocated.
 */
/* ------------------------------------------------------------------*/
void *NSAlloc(unsigned long requestedLen)
{
    // TODO: Implement memory allocation algorithm here, returning address of the
    // memory block allocated, cast as (void *).
    // Implement the memory allocation algorithms as shown in the lecture notes.

    TMemoryNode *node = findFreeMemory(requestedLen);
    if (!node) {
        return NULL;
    }

    /** If they fit exactly, there is no need to split memory slot **/
    if(requestedLen == node->len) {
        node->allocated = 1;
        
        // as instructed by handout
        _allocated_length = requestedLen;

        if (DEBUG_MODE == 1) 
            printMemoryMap();

        return (void *) node->startAddress;
    }

    // split such that this is node contains remaining memory left from slot
    unsigned long newNodefreeMem = node->len - requestedLen;
    unsigned long newNodeAddress = node->startAddress + requestedLen;
    TMemoryNode *newNodeNext = node->next;
    // release spare memory
    TMemoryNode* newNodePtr = newNode(newNodeAddress, newNodefreeMem, 0);
    newNodePtr->next = newNodeNext;
    newNodePtr->prev = node;
    insertNode(newNodePtr);

    // update the taken-up node
    node->next = newNodePtr;
    node->len = requestedLen;
    node->allocated = 1;

    // as instructed by handout
    _allocated_length = requestedLen;

    if (DEBUG_MODE == 1) 
        printMemoryMap();

    return (void *) node->startAddress;
  
    // IMPORTANT: Set the global variable _allocated_length to the number of bytes
    //            of memory you are allocating in this request.
    
    // Dummy return statement to suppress compiler errors
    
    // ENSURE THAT YOUR CODE IS PROPERLY DOCUMENTED!!

}


/* ----------------------------------------------------------------*/
/**
 * @brief This function coalesces to the left in a greedy fashion.  It is a
 * helper function for NSFree, to make code more readable.
 *
 * As a method of defensive coding, non-existent nodes are checked.  Function
 * returns null for head or tail pointers.
 *
 * Necesary safeguards and escapes are done for hardening and increased
 * performance.
 *
 * @param location The TMemoryNode representation to coalesce
 *
 * @return The merged node, with all prev and next pointers defined.
 */
/* ------------------------------------------------------------------*/
TMemoryNode* mergeLeft(TMemoryNode *location) {
    //safeguard
    if (location->prev == NULL) return NULL;
    if (location->prev->allocated == 1) return location;

    // do the linking and merging for linked list
    TMemoryNode *currNode = location->prev;
    TMemoryNode *nextNode = location->next;
    mergeNodes(currNode);
    currNode->next = nextNode;
    return currNode;

}



/* ----------------------------------------------------------------*/
/**
 * @brief This function coalesces to the right in a greedy fashion.  It is a
 * helper function for NSFree, to make code more readable.
 *
 * Necesary safeguards and escapes are done for hardening and increased
 * performance.
 *
 * As a method of defensive coding, non-existent nodes are checked.  Function
 * returns null for head or tail pointers.
 *
 * @param location The TMemoryNode representation to coalesce
 *
 * @return The merged node, with all prev and next pointers defined.
 */
/* ------------------------------------------------------------------*/
TMemoryNode* mergeRight(TMemoryNode *location) {
    //safeguard
    if (location->next == NULL) return NULL;
    if (location->next->allocated == 1) return location;

    // do the linking and merging for linked list
    TMemoryNode *nextNode = location->next->next;
    mergeNodes(location);
    location->next = nextNode;
    return location;

}


/* ----------------------------------------------------------------*/
/**
 * @brief This is a helper function for NSFree to convert the pointer to free
 * into a parseable TMemoryNode pointer.
 *
 * @param pointerToFree The raw pointer to free 
 *
 * @return A TMemoryNode representation key of the node in the memory map.  If
 * non-existent, returns NULL.
 */
/* ------------------------------------------------------------------*/
TMemoryNode* findClosestNode(void* pointerToFree) {
    TMemoryNode* curr = _head;
    unsigned long ptrAddress = (unsigned long) pointerToFree;

    while(curr != NULL) {
        if(curr->startAddress == ptrAddress)  // yay found
            return curr;

        curr = curr->next;
    }

    return NULL;
}



/* ----------------------------------------------------------------*/
/**
 * @brief When called, frees memory specified by the pointer location 
 *
 * Pre-condition: pointerToFree = Starting address of memory segment to free
 * Contracts: 
 *      Pointer to free is a valid memory location.
 *      Pointer to free occurs on a memory address block (i.e. it does
 *      not refer to the middle of an address block
 *
 * Post-condition: Memory segment is freed
 *
 * NSFree first frees the pointer specified.  If possible, all adjacent nodes
 * to the left are coalesced in a greedy fashion.  Then, all adjacent nodes to The 
 * right are coalesced in a greedy fashion.
 *
 * Necessary boundary conditions are checked in this function.
 *
 * @param pointerToFree The pointer to free.
 */
/* ------------------------------------------------------------------*/
void NSFree(void *pointerToFree)
{
    // Implement the free memory routine.
    // Locate the node corresponding to the memory to free, set the allocated flag
    // to false to "deallocate" the memory, and do a merge if possible with neighboring
    // "free memory" nodes.
    
    // ENSURE THAT YOUR CODE IS PROPERLY DOCUMENTED!!
    TMemoryNode* nodeToFree = findClosestNode(pointerToFree);
    nodeToFree->allocated = 0;

    while(nodeToFree->prev != NULL && nodeToFree->prev->allocated == 0) {
        nodeToFree = mergeLeft(nodeToFree);
    }

    while(nodeToFree->next != NULL && nodeToFree->next->allocated == 0) {
        nodeToFree = mergeRight(nodeToFree);
    }

    // print memory map, if desired
    if (DEBUG_MODE == 1) 
        printMemoryMap();

}



