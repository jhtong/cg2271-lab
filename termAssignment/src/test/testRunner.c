#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdio.h>
#include <math.h>
#include "../main/memmang.h"
#include "../main/timing.h"

/* A test case that does nothing and succeeds. */
static void null_test_success(void **state) {
    (void) state; /* unused */
}

static void null_test_success2(void **state) {
    (void) state; /* unused */
}

static void test_first_fit(void **state) {

	/** NOTE: Change MEMMANG_TYPE IN memmang.h **/

	initializeMemory();
    TMemoryNode *freeNode = findFreeMemory(1);

	#if MEMMANG_TYPE==MEMMANG_BESTFIT
    assert_int_equal(freeNode->len, 512);
    //TODO: INCLUDE MORE TESTS AFTER ALLOC IS WRITTEN

	#elif MEMMANG_TYPE==MEMMANG_WORSTFIT
    assert_int_equal(freeNode->len, 512);


	#elif MEMMANG_TYPE==MEMMANG_FIRSTFIT
    assert_int_equal(freeNode->len, 512);

	#endif
}


int main(void) {
    const UnitTest tests[] = {
        unit_test(null_test_success),
        unit_test(test_first_fit)
    };
    return run_tests(tests);
}
