#include <stdio.h>
#include <pthread.h>

int glob;
pthread_t thread[10]; //qns7
pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;

void *child(void *t)
{
	pthread_mutex_lock(&mutex);
	// Increment glob by 1, wait for 1 second, then increment by 1 again.
	printf("Child %d entering. Glob is currently %d\n", t, glob);
	glob++;
	pthread_mutex_unlock(&mutex);
	sleep(1);
	pthread_mutex_lock(&mutex);
	glob++;
	printf("Child %d exiting. Glob is currently %d\n", t, glob);
	pthread_mutex_unlock(&mutex);
	pthread_exit(NULL); //qns 7
}

int main()
{
	int i;
	glob=0;
	for(i=0; i<10; i++)
	{
		pthread_create(&thread[i], NULL, child, (void *) i); //qns 7
	}
	pthread_join(thread[9], NULL);
	printf("Final value of glob is %d\n", glob);
	pthread_mutex_destroy(&mutex);
	return 0;
}
